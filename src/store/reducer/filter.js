const initialState = {
    search: false,
    fruits: ["apple", "banana", "grapes", "apricot", "avocado", "coconut", "cherry", "pineapple"],
    vegetables: ["garlic", "basil", "aubergine", "broccoli", "carrot", "beet", "cappage"],
    brand: [],
    rating: [],
    price: [0,100]
}


export default function reducer(state = initialState, action) {
    switch (action.type) {

        case "SEARCH_PRODUCT": {
            return {
                ...state,
                search: action.payload.value
            }
        }

        case "SELECT_FRUIT": {
            return {
                ...state,
                fruits: action.payload.fruits
            }
        }

        case "SELECT_VEGETABLE": {
            return {
                ...state,
                vegetables: action.payload.vegetables
            }
        }
        case "SELECT_BRANDS": {
            return {
                ...state,
                brand: action.payload.brand
            }
        }
        case "SELECT_PRICE": {
            return {
                ...state,
                price: action.payload.price
            }
        }

        case "SELECT_RATING": {
            return {
                ...state,
                rating: action.payload.rating
            }
        }
        default: {
            return state
        }
    }
}