const initialState = {
    country: false,
    farm: false,
    bio: false,
    popular: false,
    cheapest: false
}


export default function reducer(state = initialState, action) {
    switch (action.type) {
        case "FILTER_BY_COUNTRY": {
            return {
                ...state,
                country: action.payload.country
            }
        }
        case "FILTER_BY_FARM": {
            return {
                ...state,
                farm: action.payload.checked
            }
        }
        case "FILTER_BY_BIO": {
            return {
                ...state,
                bio: action.payload.checked
            }
        }
        case "SORT_BY_POPULAR": {
            return {
                ...state,
                popular: action.payload.checked
            }
        }
        case "SORT_BY_PRICE": {
            return {
                ...state,
                cheapest: action.payload.checked
            }
        }

        default: {
            return state
        }
    }
}