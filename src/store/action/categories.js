
export const selectFarm = (checked) => {
    return {
        type: "FILTER_BY_FARM",
        payload: {
            checked: checked
        }
    }
}

export const selectBio = (checked) => {
    return {
        type: "FILTER_BY_BIO",
        payload: {
            checked: checked
        }
    }
}

export const sortPopular = (checked) => {
    return {
        type: "SORT_BY_POPULAR",
        payload: {
            checked: checked
        }
    }
}


export const sortPrice = (checked) => {
    return {
        type: "SORT_BY_PRICE",
        payload: {
            checked: checked
        }
    }
}

export const search = (value) => {
    return {
        type: "SEARCH_PRODUCT",
        payload: {
            value: value
        }
    }
}



export const selectCountry = (country) => {
    return {
        type: "FILTER_BY_COUNTRY",
        payload: {
            country: country
        }
    }
}