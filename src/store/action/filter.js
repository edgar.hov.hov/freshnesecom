

export const search = (value) => {
    return {
        type: "SEARCH_PRODUCT",
        payload: {
            value: value
        }
    }
}


export const selectFruit = (fruits) => {
    return {
        type: "SELECT_FRUIT",
        payload: {
            fruits: fruits
        }
    }
}

export const selectVegetable = (vegetables) => {
    return {
        type: "SELECT_VEGETABLE",
        payload: {
            vegetables: vegetables
        }
    }
}

export const selectBrand = (brand) => {
    return {
        type: "SELECT_BRANDS",
        payload: {
            brand: brand
        }
    }
}

export const selectPrice = (price) => {
    return {
        type: "SELECT_PRICE",
        payload: {
            price: price
        }
    }
}


export const selectRating = (rating) => {
    return {
        type: "SELECT_RATING",
        payload: {
            rating: rating
        }
    }
}





