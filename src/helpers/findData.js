import _ from 'lodash'

export function filterByCountry(country, data) {
    if (country !== "All Countries") {
        return _.filter(data, {delivery: country})
    } else {
        return data
    }
}

export function filterByProduction(farm, bio, data) {
    if (farm && bio) {
        return data
    }
    if (farm) {
        return _.filter(data, {production: "farm"})
    }
    if (bio) {
        return _.filter(data, {production: "bio"})
    }
}

export function sortByRating(data) {
    const sortRating = (data.sort(function (a, b) {
        return b.rating - a.rating;
    }))
    return sortRating
}

export function sortByPrice(data) {
    const sortPrice = (data.sort(function (a, b) {
        return a.price - b.price;
    }))
    return sortPrice
}

export function searchInData(value, data) {
    const find = []
    data.filter(d => d.name.toLowerCase().includes(value)).map((p) => {
        find.push(p)
    })
    return find
}

export function filterCategory(fruits, vegetables, data) {
    const filter = []
    if (fruits.length === 8 && vegetables.length === 7) {
        return data
    }
    for (let i in data) {
        for (let f in fruits) {
            if (data[i].name === fruits[f]) {
                filter.push(data[i])
            }
        }
    }
    for (let i in data) {
        for (let v in vegetables) {
            if (data[i].name === vegetables[v]) {
                filter.push(data[i])
            }
        }
    }
    return filter
}

export function filterByBrand(brand, data) {
    const filter = []
    if (brand.length === 0) {
        return data
    }
    for (let i in data) {
        for (let b in brand) {
            if (data[i].brand === brand[b]) {
                filter.push(data[i])
            }
        }
    }
    return filter
}

export function filterByPrice(price, data) {
    const filter = []
    for (let i in data) {
        if (price[0] < parseInt(data[i].price)  && parseInt(data[i].price) < price[1]) {
            filter.push(data[i])
        }
    }
    return filter
}

export function filterByRating(rating, data) {
    const filter = []
    if (rating.length === 0) {
        return data
    }
    for (let i in data) {
        for (let r in rating) {
            if (+data[i].rating === +rating[r]) {
                filter.push(data[i])
            }
        }
    }
    return filter
}
