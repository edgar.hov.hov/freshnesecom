import React from 'react';

function Pagination({dataPage, totalData, paginate, page, data}) {
    const pageNumber = []
    for (let i = 1; i <= Math.ceil(totalData / dataPage); i++) {
        pageNumber.push(i)
    }

    if (data.length === 0) {
        paginate(1)
    }
    return (
        <div>
            <ul className="pagination">
                {pageNumber.length > 1 ?
                    <li><a href="#" onClick={event => page === 1 ? paginate(pageNumber.length) : paginate(page - 1)}>«</a>
                    </li> : null}
                {pageNumber.map(number => (
                    <li className="pageItem" key={number}>
                        <a href="#" id={number} className={`${page === number ? "page" : null}`} onClick={(ev) => {
                            paginate(number)
                        }}>
                            {number}
                        </a>
                    </li>
                ))}
                {pageNumber.length > 1 ?
                    <li><a href="#" onClick={event => page === pageNumber.length ? paginate(1) : paginate(page + 1)}>»</a>
                    </li> : null}
            </ul>
        </div>
    );
}

export default Pagination;
//
// <li className="pageItem" key={number}>
//     <a href="#" className={`pageLink ${page === number? "page" : null}`} onClick={(ev) => {
//         paginate(number)
//     }}>
//         {number}
//     </a>
// </li>