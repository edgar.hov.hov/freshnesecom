import React, {useState} from 'react';
import Pagination from "./Pagination";
import ProductsList from "./ProductsList";
import _ from "lodash"
import {useSelector} from "react-redux";
import product from '../data/products.json'
import {
    filterByBrand,
    filterByCountry, filterByPrice,
    filterByProduction, filterByRating,
    filterCategory,
    searchInData,
    sortByPrice,
    sortByRating
} from "../helpers/findData";


function Products(props) {
    const {categories, filter} = useSelector(function (state) {
        return state
    })

    let data = _.clone(product)
    if (categories.country) {
        data = filterByCountry(categories.country, data)
    }
    if (categories.bio || categories.farm) {
        data = filterByProduction(categories.farm, categories.bio, data)
    }
    if (categories.popular) {
        data = sortByRating(data)
    }

    if (categories.cheapest) {
        data = sortByPrice(data)
    }
    if (filter.search) {
        data = searchInData(filter.search, data)
    }
    if (filter.fruits || filter.vegetables) {
        data = filterCategory(filter.fruits, filter.vegetables, data)
    }
    if (filter.brand) {
        data = (filterByBrand(filter.brand, data))
    }
    if (filter.price) {
        data = filterByPrice(filter.price, data)
    }

    if (filter.rating){
       data =  filterByRating(filter.rating, data)
    }

    const [currentPage, setCurrentPage] = useState(1)
    const [dataPage] = useState(6)

    const lastDataIndex = currentPage * dataPage
    const firstDataIndex = lastDataIndex - dataPage
    const currentData = data.slice(firstDataIndex, lastDataIndex)
    const paginate = pageNumber => setCurrentPage(pageNumber)
        return (
        <div>
            <ProductsList data={currentData}/>
            <Pagination data={currentData} dataPage={dataPage} page={currentPage} totalData={data.length} paginate={paginate}/>
        </div>
    );
}

export default Products;