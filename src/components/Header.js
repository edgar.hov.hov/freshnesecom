import React from 'react';
import {AiOutlineUser} from 'react-icons/ai'
import {GrBasket} from 'react-icons/gr'
import {useDispatch} from "react-redux";
import {search} from "../store/action/categories";

function Header(props) {
    const dispatch = useDispatch()

    return (
        <>
        <header className='header'>
            <div className='container'>
                <div className='header-content'>
                    <div className='logo'>
                        <a href="/"><h2 className='header-logo'>Freshnesecom</h2></a>
                    </div>
                    <div className='select-search'>
                        <form className='search-Form'>
                            <input className='search-input' placeholder='Search products ...' onChange={(ev) => dispatch(search(ev.target.value))}  type="text"/>
                        </form>
                    </div>
                    <div className='icons'>
                        <a href=""><AiOutlineUser/></a>
                        <a href=""><GrBasket/></a>
                    </div>

                </div>
            </div>
        </header>

</>
)
    ;
}

export default Header;