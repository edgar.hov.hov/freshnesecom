import React from 'react';

function Header(props) {
    return (
        <div>
            <footer className='footer'>
                <div className="footer-container">
                    <div className='footer-content'>
                        <div>
                            <h3>Get in touch</h3>
                            <ul>
                                <li>About Us</li>
                                <li>Careers</li>
                                <li>Press Releases</li>
                                <li>Blog</li>
                            </ul>
                        </div>
                        <div>
                            <h3>Connections</h3>
                            <ul>
                                <li>Facebook</li>
                                <li>Twitter</li>
                                <li>Instagram</li>
                                <li>Youtube</li>
                            </ul>
                        </div>
                        <div>
                            <h3>Earnings</h3>
                            <ul>
                                <li>Become an Affiliate</li>
                                <li>Advertise your product</li>
                                <li>Sell on Market</li>
                            </ul>
                        </div>
                        <div>
                            <h3>Account</h3>
                            <ul>
                                <li>Your account</li>
                                <li>Returns Centre</li>
                                <li>Chat with us</li>
                                <li>Help</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}

export default Header;