import React, {useState} from 'react';
import Box from '@mui/material/Box';
import Slider from '@mui/material/Slider'
import {useDispatch} from "react-redux";
import {selectPrice} from "../store/action/filter";

function valuetext(value) {
    return `${value}°C`;
}

export default function RangeSlider() {
    const values = []
    const [minValue, setMinValue] = useState(0);
    const [maxValue, setMaxValue] = useState(100);
    const dispatch = useDispatch()

    const handleChange = (event, newValue) => {
        setMinValue(newValue[0])
        setMaxValue(newValue[1])
    };

    const minValueChange = (ev) => {
        setMinValue(ev.target.value)
    }
    const maxValueChange = (ev) => {
        setMaxValue(ev.target.value)
    }

    values.push(minValue, maxValue)
    setTimeout(() => {
        dispatch(selectPrice([minValue, maxValue]))
    }, 500)


    return (
        <div>
            <Box sx={{width: "92%"}}>
                <Slider
                    getAriaLabel={() => 'Temperature range'}
                    value={values}
                    style={{color: "#6A983C"}}
                    onChange={handleChange}
                    valueLabelDisplay="auto"
                    getAriaValueText={valuetext}
                />
            </Box>
            <div className="price-label-div">
                <label htmlFor="minValue">Min</label>
                <label htmlFor="maxValue">Max</label>
            </div>
            <div className="price-values-div">
                <input  name="minValue" type="text" className="minValue-input" value={values[0]} onChange={ev => {
                    minValueChange(ev)
                }}/>
                <input  name="maxValue" type="text" className="maxValue-input" value={values[1]} onChange={ev => {
                    maxValueChange(ev)
                }}/>
            </div>
        </div>

    );
}


