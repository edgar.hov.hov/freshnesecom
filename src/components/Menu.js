import React, {useState} from 'react';
import _ from 'lodash'
import {AiTwotoneStar, AiOutlineStar, AiOutlineDown, AiOutlineUp} from 'react-icons/ai'
import data from '../data/products.json'
import {useDispatch, useSelector} from "react-redux";
import {selectFruit, selectVegetable, selectBrand, selectRating} from "../store/action/filter";

function Menu(props) {
    const {fruits, vegetables, brand, rating} = useSelector(store => store.filter)
    const [showFruits, setShowFruits] = useState(true)
    const [showVegetables, setShowVegetables] = useState(true)
    const dispatch = useDispatch()
    const fruitNames = []
    const vegetablesNames = []
    const fruitsArr = []
    const vegetablesArr = []
    const brands = []
    for (let i in data) {
        const find = _.find(fruitNames, {name: data[i].name}) || _.find(vegetablesNames, {name: data[i].name})
        const findBrand = brands.includes(data[i].brand)
        if (!find) {
            if (data[i].category === "fruit") {
                fruitNames.push(data[i])
            } else {
                vegetablesNames.push(data[i])
            }
        }
        if (!findBrand) {
            brands.push(data[i].brand)
        }
    }

    for (let i in fruitNames) {
        fruitsArr.push(fruitNames[i].name)
    }

    for (let i in vegetablesNames) {
        vegetablesArr.push(vegetablesNames[i].name)
    }

    const handleFruit = (ev) => {
        const i = fruits.findIndex(fruit => fruit === ev.target.id);
        if (i > -1) {
            fruits.splice(i, 1)
        } else {
            fruits.push(ev.target.id)
        }
        dispatch(selectFruit(fruits))
    }


    const handleVegetable = (ev) => {
        const i = vegetables.findIndex(vegetables => vegetables === ev.target.id);
        if (i > -1) {
            vegetables.splice(i, 1)
        } else {
            vegetables.push(ev.target.id)
        }
        dispatch(selectVegetable(vegetables))
    }

    const handleBrand = (ev) => {
        const i = brand.findIndex(vegetables => vegetables === ev.target.id);
        if (i > -1) {
            brand.splice(i, 1)
        } else {
            brand.push(ev.target.id)
        }
        dispatch(selectBrand(brand))
    }

    const handleRating = (ev) => {
        const i = rating.findIndex(rating => rating === ev.target.id);
        if (i > -1) {
            rating.splice(i, 1)
        } else {
            rating.push(ev.target.id)
        }
        dispatch(selectRating(rating))
    }
    return (
        <>
            <h3 className="menu-tittle">Categories</h3>
            <ul>
                <li>
                    <div className="fruit-category-div">
                        <form>
                            <input defaultChecked className="menu-checkbox" id="fruits"
                                   type="checkbox" onChange={event => {
                                if (event.target.checked) {
                                    dispatch(selectFruit(fruitsArr))
                                } else dispatch(selectFruit([]))
                                setShowFruits(true)
                            }}/>
                            <label className="menu-categories-label" htmlFor="fruits">Fruits</label>
                        </form>
                        <span onClick={event => {
                            setShowFruits(!showFruits)
                        }}>
                           {showFruits ? <AiOutlineUp/> : <AiOutlineDown/>}
                        </span>
                    </div>
                    <ul>
                        {fruitNames.map((fruit) => (
                            <li key={fruit.id} className={showFruits ? "fruit-categories" : "displayNone"}>
                                <form key={fruit.id}>
                                    <input checked={fruits.includes(fruit.name)} className="menu-checkbox"
                                           onChange={ev => handleFruit(ev)} key={fruit.name} id={fruit.name}
                                           type="checkbox"/>
                                    <label className="menu-categories-label"
                                           id={fruit.name}
                                           htmlFor={fruit.name}>
                                        {fruit.name}
                                    </label>
                                </form>
                            </li>
                        ))}
                    </ul>
                </li>
                <li>
                    <form>
                        <input defaultChecked className="menu-checkbox" id="Vegetables" onChange={(event) => {
                            if (event.target.checked) {
                                dispatch(selectVegetable(vegetablesArr))
                            } else dispatch(selectVegetable([]))
                            setShowVegetables(true)
                        }} type="checkbox"/>
                        <label className="menu-categories-label" htmlFor="Vegetables">Vegetables</label>
                        <span className={"vegetables-span"} onClick={event => {
                            setShowVegetables(!showVegetables)
                        }}>
                           {showVegetables ? <AiOutlineUp/> : <AiOutlineDown/>}
                        </span>
                    </form>
                    <ul>
                        {vegetablesNames.map((veg) => (
                            <li className={showVegetables ? "fruit-categories" : "displayNone"}>
                                <form key={veg.id}>
                                    <input checked={vegetables.includes(veg.name)}
                                           onChange={(ev) => handleVegetable(ev)}
                                           className="menu-checkbox" id={veg.name} type="checkbox"/>
                                    <label className="menu-categories-label" htmlFor={veg.name}>{veg.name}</label>
                                </form>
                            </li>
                        ))}
                    </ul>
                </li>
            </ul>
            <h3 className="menu-tittle-brands">Brands</h3>
            <ul>
                {brands.map((b, i) => (
                    <li>
                        <form key={i + 156464}>
                            <input className="menu-checkbox" id={b}
                                   onChange={event => handleBrand(event)} type="checkbox"/>
                            <label className="menu-categories-label" htmlFor={b}>{b}</label>
                        </form>
                    </li>
                ))}
            </ul>
            <h3 className="menu-tittle-brands">Rating</h3>
            <ul>
                {_.range(0, 5).map((p) => (
                    <li>
                        <form key={p + 66}>
                            <input onChange={(ev) => handleRating(ev)} className="menu-checkbox" id={5 - p}
                                   type="checkbox"/>
                            <label id={p + 564564} className="menu-categories-label" htmlFor={5 - p}>
                                {_.range(0, 5 - p).map((n) => (
                                    <AiTwotoneStar id={p + 12654654789} color={"#FDBC15"}/>
                                ))}
                                {_.range(0, p).map((n) => (
                                    <AiOutlineStar id={p + 66582546} color={"#D1D1D1"}/>
                                ))}
                            </label>
                        </form>
                    </li>
                ))}
            </ul>
            <h3 className="menu-tittle-brands">Price</h3>
        </>
    );
}

export default Menu;