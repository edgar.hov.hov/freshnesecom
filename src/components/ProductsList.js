import React from 'react';
import _ from "lodash";
import {AiOutlineStar, AiTwotoneStar} from "react-icons/ai";

function ProductsList({data}) {

    return (
        <div>
             {data.map((d) => (
            <div className="product-div">
                <div className="img-div">
                    <img src={`/images/${d.img}.jpg`} alt="photo"/>
                </div>
                <div className="about-product-div">
                    <h2>{d.name}</h2>
                    <div className="product-parameters">
                        <div>
                            <ul>
                                <li key={1}>Rating</li>
                                <li key={2}>Type</li>
                                <li key={3}>Brand</li>
                                <li key={4}>Production</li>
                                <li key={5}>Delivery</li>
                            </ul>
                        </div>
                        <div>
                            <ul>
                                <li key={6}> {_.range(0, +d.rating).map((p, i) => (
                                    <AiTwotoneStar key={i} color={"#FDBC15"}/>
                                ))}{_.range(0, 5 - (+d.rating)).map((p) => (
                                    <AiOutlineStar color={"#D1D1D1"}/>
                                ))}
                                </li>
                                <li key={7}>{d.category}</li>
                                <li key={8}>{d.brand}</li>
                                <li key={9}>{d.production}</li>
                                <li key={10}>{d.delivery}</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div className="buy-product">
                    <h2>{d.price} USD </h2>
                    <span className="span-delete">{parseInt(d.price) + 10}.99 USD<br/></span>
                    <div className="shipping">
                        <span>Free Shipping <br/> Delivery in 1 day</span>
                    </div>
                    <div className="buy-div">
                        <button>
                            Buy product
                        </button>
                    </div>
                </div>
            </div>
        ))}
        </div>
    );
}

export default ProductsList;