import React from 'react';
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Categories from "./pages/Categories";


function App(props) {

    return (
        <>
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<Categories/>}/>
                </Routes>
            </BrowserRouter>
        </>
    );
}

export default App;