import React, {useState} from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";
import countries from "../data/countries.json"
import Menu from "../components/Menu";
import Range from "../components/Range";
import {StyledEngineProvider} from '@mui/material/styles'
import Products from "../components/Products";
import {useDispatch} from "react-redux";
import {selectBio, selectCountry, selectFarm, sortPopular, sortPrice} from "../store/action/categories";


function Categories(props) {
    const [popularCheck, setPopularCheck] = useState(false)
    const [cheapestCheck, setCheapestCheck] = useState(false)

    const toggleCheck = (ev) => {
        if (ev.target.htmlFor === "popular") {
            setPopularCheck(!popularCheck)
        }
        if (ev.target.htmlFor === "cheapest") {
            setCheapestCheck(!cheapestCheck)
        }
    }
    const dispatch = useDispatch()


    return (
        <>
            <Header/>
            <div className='container'>
                <h1 className='categories-tittle'>Fruit and vegetables</h1>
                <div className='checkboxes'>
                    <div>
                        <label className="custom-checkbox" tab-index="0" aria-label="Checkbox Label">
                            <input id="farm" onChange={(ev) => dispatch(sortPopular(ev.target.checked))}
                                   type="checkbox"/>
                            <span id="farm" className="checkmark"></span>
                            <span id={"farm"} className="label">Most Popular</span>
                        </label>


                        <label className="custom-checkbox" tab-index="0" aria-label="Checkbox Label">
                            <input id="bio" onChange={(ev) => dispatch(sortPrice(ev.target.checked))}  type="checkbox"/>
                            <span id="bio" className="checkmark"></span>
                            <span id="bio" className="label">Cheapest</span>
                        </label>
                    </div>
                    <div>
                        <label className="custom-checkbox" tab-index="0" aria-label="Checkbox Label">
                            <input id="farm" onChange={(ev) => dispatch(selectFarm(ev.target.checked))}
                                   type="checkbox"/>
                            <span id="farm" className="checkmark"></span>
                            <span id={"farm"} className="label">Farm Product</span>
                        </label>


                        <label className="custom-checkbox" tab-index="0" aria-label="Checkbox Label">
                            <input id="bio" onChange={(ev) => dispatch(selectBio(ev.target.checked))} type="checkbox"/>
                            <span id="bio" className="checkmark"></span>
                            <span id="bio" className="label">Bio Product</span>
                        </label>
                    </div>
                    <div>
                        <select className="select-countries" onChange={(ev) => dispatch(selectCountry(ev.target.value))}
                                name="countries">
                            <option>All Countries</option>
                            {countries.map((c) => (
                                <option key={c.key}>{c.name}</option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="content">
                    <div className='left-menu'>
                        <Menu/>
                        <StyledEngineProvider injectFirst>
                            <Range/>
                        </StyledEngineProvider>
                    </div>
                    <div className="products">
                        <Products/>
                    </div>
                </div>

            </div>
            <Footer/>
        </>
    );
}

export default Categories;