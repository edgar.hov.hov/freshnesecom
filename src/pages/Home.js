import React from 'react';
import Header from "../components/Header";
import Footer from "../components/Footer";

function Home(props) {
    return (
        <>
            <Header/>
            <Footer/>
        </>
    );
}

export default Home;